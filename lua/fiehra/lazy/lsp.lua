
return {
	"neovim/nvim-lspconfig",
	dependencies = {
		"williamboman/mason.nvim",
		"williamboman/mason-lspconfig.nvim",
		"hrsh7th/cmp-nvim-lsp",
		"hrsh7th/cmp-buffer",
		"hrsh7th/cmp-path",
		"hrsh7th/cmp-cmdline",
		"hrsh7th/nvim-cmp",
		"L3MON4D3/LuaSnip",
		"saadparwaiz1/cmp_luasnip",
		"j-hui/fidget.nvim",
	},




	config = function()
		local cmp = require('cmp')
		local cmp_lsp = require("cmp_nvim_lsp")
		local capabilities = vim.tbl_deep_extend(
			"force",
			{},
			vim.lsp.protocol.make_client_capabilities(),
			cmp_lsp.default_capabilities())

		require("fidget").setup({})
		require("mason").setup()
		require("mason-lspconfig").setup({
			ensure_installed = {
				"lua_ls",
				"ts_ls",
				"angularls",
				"eslint",
			},
			handlers = {
				function(server_name) -- default handler (optional)
					require("lspconfig")[server_name].setup {
						capabilities = capabilities
					}
				end,
				["lua_ls"] = function()
					require("lspconfig").lua_ls.setup {
						capabilities = capabilities,
						settings = {
							diagnostics = {
								globals = { "vim" },
							},
						},
					}
				end,
				["ts_ls"] = function()
					require("lspconfig").ts_ls.setup {
						capabilities = capabilities,
						init_options = {
							preferences = {
								disableSuggestions = true,
							},
						},
					}
				end,
				["angularls"] = function()
					require("lspconfig").angularls.setup {
						capabilities = capabilities,
					}
				end,
				["eslint"] = function()
					require("lspconfig").eslint.setup {
						capabilities = capabilities,
					}
				end,
			}
		})

		local cmp_select = { behavior = cmp.SelectBehavior.Select }

		cmp.setup({
			snippet = {
				expand = function(args)
					require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
				end,
			},
			mapping = cmp.mapping.preset.insert({
				['<s-tab>'] = cmp.mapping.select_prev_item(cmp_select),
				['<tab>'] = cmp.mapping.select_next_item(cmp_select),
				['<CR>'] = cmp.mapping.confirm({ select = true }),
				['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }), -- Trigger completion
			}),
			sources = cmp.config.sources({
				{ name = 'nvim_lsp' },
				{ name = 'luasnip' }, -- For luasnip users.
				}, {
					{ name = 'buffer' },
			})
		})

		-- Start Godot server if project.godot is present
		local projectfile = vim.fn.getcwd() .. "/project.godot"
		if vim.fn.filereadable(projectfile) == 1  then
			vim.fn.serverstart "./godothost"
			print("Godot server started")
		end

		vim.diagnostic.config({
			-- update_in_insert = true,
			float = {
				focusable = false,
				style = "minimal",
				border = "rounded",
				source = "always",
				header = "",
				prefix = "",
			},
		})
	end
}
