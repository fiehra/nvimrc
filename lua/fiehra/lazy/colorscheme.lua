function ColorMyPencils(color)
color = color or "nordic"
	vim.cmd.colorscheme(color)

	vim.api.nvim_set_hl(0, 'Normal', { bg = 'none' })
	vim.api.nvim_set_hl(0, 'NormalFloat', { bg = 'none' })
end

return {
	{
		'AlexvZyl/nordic.nvim',
		config = function()
			require("nordic").setup({
				cursorline = { theme = "light" },
			})
		end
	},
	{
		'ramojus/mellifluous.nvim',
		config = function()
			require'mellifluous'.setup({}) -- optional, see configuration section.
		end
	},
	{
		'olivercederborg/poimandres.nvim',
		config = function()
			require('poimandres').setup({})
		end
	},
	{
		"folke/tokyonight.nvim",
		config = function()
			require("tokyonight").setup({})
		end
	},
	{
		"catppuccin/nvim", name = "catppuccin",
		config = function()
			require("catppuccin").setup({
			})
		end
	},
	{
		"rose-pine/neovim",
		name = "rose-pine",
		config = function()
			require('rose-pine').setup({
			})
			ColorMyPencils()
		end
	},
}
