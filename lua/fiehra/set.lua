vim.opt.guicursor = ''

vim.api.nvim_set_option('number', true)
vim.api.nvim_set_option('relativenumber', true)

vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2
vim.opt.expandtab = true
vim.opt.smartindent = true

vim.g.netrw_banner = 0
vim.opt.undofile = true

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undofile = true

vim.opt.hlsearch = true
vim.opt.incsearch = true

vim.opt.scrolloff = 8
vim.opt.clipboard = "unnamedplus"
