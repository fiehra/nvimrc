#nvim
##### count - command - movement ###### 

hjkl ---- move cursor
w ---- move cursor word
b ---- move cursor word back
0 ---- go to start of line
$ ---- end of line
y ---- yank the fuck out of it
yy ---- superyank the fuck out of the line
d ---- delete
dd ---- delete the fuck out of the line
D ---- delete from cursor to end of line
p ---- paste
shift + p ---- paste without replacing the paste buffer
c ---- correct
	---- cw   correct word?
C ---- correct rest of line
G ---- end of file
gg ---- beginning of file
/ ---- search
    ctrl-t / ctrl-g to jump to next search result without leaving the searchmode
    n to jump to next result when outside of search mode
f ---- forward to next character
F ---- BACK to next character
    ---- f(  --> jump to next paranthesis
; ---- repeat f command f + character
, ---- repeat command but backwards 
t ---- jumps to but not on next character
T ---- jumps BACK but not on next character
    ---- t(  --> jump to next paranthesis
o ---- jump to other side on visual selection
r ---- replace current character
i ---- insert mode
I ---- insert beginning of line
a ---- append
A ---- append end of line
v ---- visual mode  (command and movement switched  ----> ##### movement - command)
u ---- undo
esc ---- escape mode
V% ---- mark code inside brackets
R ---- rename file / filepath
C-d ---- jump half page down
C-u ---- jump half page up
viw ---- into visualmode and select word where cursor is
vi( { ---- into visual mode and select everything inside the braces
va( { ---- into visual mode and select everything inside the braces and the braces
C-a ---- increment number under cursor
C-x ---- decrement number under cursor


For example, to search for the first occurrence of the string ‘foo’ in the current line and replace it with ‘bar’, you would use:
:s/foo/bar/
To replace all occurrences of the search pattern in the current line, add the g flag:
:s/foo/bar/g
If you want to search and replace the pattern in the entire file, use the percentage character % as a range. This character indicates a range from the first to the last line of the file:
:%s/foo/bar/g

##### remaps ######
---- global
<leader>fl ---- show netrw file list
<leader>gd ---- go to definition
<leader>gD ---- go to declaration

---- telescope:
<leader>pf ---- find files project
C-p ---- find files git
<leader>ps ---- find files string
<leader>pw ---- search word under cursor

---- nvim default keymaps
Ctrl + a / x / f / b / d / u / t

##### help ######
:h key-notation
:h
:Telescope keymaps

